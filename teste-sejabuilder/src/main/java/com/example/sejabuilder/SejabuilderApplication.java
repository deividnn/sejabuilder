package com.example.sejabuilder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SejabuilderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SejabuilderApplication.class, args);
	}

}
