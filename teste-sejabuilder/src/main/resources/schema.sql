/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  deivid
 * Created: 09/06/2020
 */

CREATE TABLE IF NOT EXISTS cliente (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(100) NOT NULL,
  cpf VARCHAR(11) NOT NULL,
  data_nascimento DATE NOT NULL
);