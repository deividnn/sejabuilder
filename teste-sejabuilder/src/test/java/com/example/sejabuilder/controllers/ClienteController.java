/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.sejabuilder.controllers;

import com.example.sejabuilder.entidades.Cliente;
import com.example.sejabuilder.repositories.ClienteRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author deivid
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/clientes")
public class ClienteController {

    private ClienteRepository clienteRepository;

    public ClienteController(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    @PostMapping
    public ResponseEntity<Cliente> salvar(@RequestBody Cliente c) {
        Cliente cli = this.clienteRepository.save(c);
        return ResponseEntity.ok(cli);
    }

    @PutMapping
    public ResponseEntity<Cliente> atualizar(@RequestBody Cliente c) {
        Cliente cli = this.clienteRepository.findById(c.getId()).get();
        BeanUtils.copyProperties(c, cli);
        this.clienteRepository.save(cli);
        return ResponseEntity.ok(cli);
    }

    @PatchMapping
    public ResponseEntity<Cliente> atualizarp(@RequestBody Cliente c) {
        Cliente cli = this.clienteRepository.findById(c.getId()).get();
        BeanUtils.copyProperties(c, cli);
        this.clienteRepository.save(cli);
        return ResponseEntity.ok(cli);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> excluir(@PathVariable int id) {
        Cliente cli = this.clienteRepository.findById(id).get();
        if (cli == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        this.clienteRepository.delete(cli);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/pesquisar")
    public Page<Cliente> search(
            @RequestParam("busca") String busca,
            @RequestParam(
                    value = "page",
                    required = false,
                    defaultValue = "0") int page,
            @RequestParam(
                    value = "size",
                    required = false,
                    defaultValue = "10") int size) {
        PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC,
                "nome");
        return this.clienteRepository.search(busca.toLowerCase(), pageRequest);

    }

    @GetMapping
    public Page<Cliente> listar() {
        PageRequest pageRequest = PageRequest.of(
                0,
                10,
                Sort.Direction.ASC,
                "nome");
        return new PageImpl<>(
                this.clienteRepository.findAll(),
                pageRequest, 10);

    }

}
