/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.sejabuilder.repositories;

import com.example.sejabuilder.entidades.Cliente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author deivid
 */
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

    @Query("FROM Cliente c "
            + "WHERE LOWER(c.cpf) like %:busca% "
            + "OR LOWER(c.nome) like %:busca%")
    Page<Cliente> search(
            @Param("busca") String busca,
            Pageable pageable);

}
